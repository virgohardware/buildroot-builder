 # LINUX VIA BUILDROOT
cd ~/
apt install -y cpio zip rsync
git clone https://github.com/vj-kumar/shakti-buildroot/
cd shakti-buildroot
make riscv64_defconfig
FORCE_UNSAFE_CONFIGURE=1 make -j $(nproc)